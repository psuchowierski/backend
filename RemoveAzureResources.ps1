﻿param(
    [Parameter(Position = 0)]
    [String[]] $resourceTypes = @('Microsoft.web/sites', 'Microsoft.Sql/servers/databases'), 

    [Parameter(Position = 1, Mandatory = $true)]
    [String] $resourceGroupName
)
Login-AzureRmAccount
$resources = Get-AzureRmResource -ResourceGroupName $resourceGroupName
foreach($resource in $resources)
{
    if($resourceTypes.Contains($resource.ResourceType))
    {
            Remove-AzureRmResource -ResourceId $resource.ResourceId
    }
}