﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarsawUM.Api.Models;

namespace WarsawUM.Threats.ViewModels.Profiles
{
    public class NotificationProfile : Profile
    {
        public NotificationProfile()
        {
            CreateMap<Notification, NotificationViewModel>()
                .ForMember(dest => dest.Status, opt =>
                {
                    opt.MapFrom(src => src.Statuses.Last().Status);
                })
                .ForMember(dest => dest.StatusDate, opt =>
                {
                    opt.MapFrom(src =>
                        DateTimeOffset.FromUnixTimeMilliseconds(src.Statuses.Last().ChangeDate).UtcDateTime);
                })
                .ForMember(dest => dest.CreateDate, opt =>
                {
                    opt.MapFrom(src => DateTimeOffset.FromUnixTimeMilliseconds(src.CreateDate).UtcDateTime);
                });
        }
    }
}
