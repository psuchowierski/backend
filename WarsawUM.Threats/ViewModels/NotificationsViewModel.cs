﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarsawUM.Threats.ViewModels
{
    public class NotificationsViewModel
    {
        public IEnumerable<NotificationViewModel> Notifications { get; set; }
    }
}
