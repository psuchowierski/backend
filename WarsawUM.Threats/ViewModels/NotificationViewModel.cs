﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarsawUM.Threats.ViewModels
{
    public class NotificationViewModel
    {
        public string District { get; set; }

        public string Event { get; set; }

        public DateTime CreateDate { get; set; }

        public string Street { get; set; }

        public string Status { get; set; }

        public DateTime StatusDate { get; set; }
    }
}
