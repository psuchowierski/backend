﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WarsawUM.Api.Interfaces;
using WarsawUM.Api.Models;
using WarsawUM.Threats.ViewModels;

namespace WarsawUM.Threats.Controllers
{
    public class NotificationController : Controller
    {
        private readonly INotificationsRepository _notificationRepository;
        private readonly IMapper _mapper;

        public NotificationController(INotificationsRepository notificationRepository, IMapper mapper)
        {
            _notificationRepository = notificationRepository;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var rawNotifications = await _notificationRepository.GetNotifications(1);
            NotificationsViewModel viewModel = new NotificationsViewModel()
            {
                Notifications = _mapper.Map<IEnumerable<Notification>, IEnumerable<NotificationViewModel>>(rawNotifications)
                                       .OrderByDescending(x => x.CreateDate)
            };
            return View(viewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
