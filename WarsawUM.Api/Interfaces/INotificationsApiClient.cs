﻿using System;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace WarsawUM.Api.Interfaces
{
    public interface INotificationsApiClient
    {
        Task<T> GetAsync<T>(string requestRelativeUrl, NameValueCollection httpParams);
    }
}
