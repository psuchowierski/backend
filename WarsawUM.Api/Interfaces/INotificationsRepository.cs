﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WarsawUM.Api.Models;

namespace WarsawUM.Api.Interfaces
{
    public interface INotificationsRepository
    {
        Task<IEnumerable<Notification>> GetNotifications(int days);
    }
}
