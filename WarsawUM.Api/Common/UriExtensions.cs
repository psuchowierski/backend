﻿using System;
using System.Collections.Specialized;
using System.Web;

namespace WarsawUM.Api.Common
{
    public static class UriExtensions
    {
        public static Uri SetQueryVal(this Uri uri, string name, object value)
        {
            NameValueCollection nvc = HttpUtility.ParseQueryString(uri.Query);
            nvc[name] = (value ?? string.Empty).ToString();
            return new UriBuilder(uri) { Query = nvc.ToString() }.Uri;
        }
    }
}