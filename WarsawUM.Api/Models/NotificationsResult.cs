﻿using Newtonsoft.Json;

namespace WarsawUM.Api.Models
{
    [JsonObject]
    public class NotificationsResult
    {
        [JsonProperty(PropertyName = "notifications")]
        public Notification[] Notifications { get; set; }

        [JsonProperty(PropertyName = "responseDesc")]
        public string ResponseDesc { get; set; }

        [JsonProperty(PropertyName = "responseCode")]
        public string ResponseCode { get; set; }
    }
}