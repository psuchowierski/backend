﻿using Newtonsoft.Json;

namespace WarsawUM.Api.Models
{
    [JsonObject]
    public class NotificationsResultContainer
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "result")] 
        public NotificationsResult Result { get; set; }
    }
}