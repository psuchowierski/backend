﻿using Newtonsoft.Json;

namespace WarsawUM.Api.Models
{
    [JsonObject]
    public class NotificationsRoot
    {
        [JsonProperty(PropertyName = "result")]
        public NotificationsResultContainer Result { get; set; }
    }
}