﻿using Newtonsoft.Json;

namespace WarsawUM.Api.Models
{
    [JsonObject]
    public class NotificationStatus
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "changeDate")]
        public long ChangeDate { get; set; }
    }
}
