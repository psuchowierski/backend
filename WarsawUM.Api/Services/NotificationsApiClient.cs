﻿using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WarsawUM.Api.Common;
using WarsawUM.Api.Interfaces;

namespace WarsawUM.Api.Services
{
    public partial class NotificationsApiClient : INotificationsApiClient
    {
        private readonly HttpClient _httpClient;
        private readonly string _apiKey;

        private Uri _baseUri { get; set; }
         
        private static JsonSerializerSettings MicrosoftDateFormatSettings
        {
            get
            {
                return new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
                };
            }
        }

        public NotificationsApiClient(Uri baseUri, string apiKey)
        {
            if (string.IsNullOrEmpty(baseUri?.ToString()) || !baseUri.IsAbsoluteUri)
            {
                throw new ArgumentException($"Wrong argument specified - {nameof(baseUri)}.");
            }
            _baseUri = baseUri;
            _httpClient = new HttpClient();
            _apiKey = apiKey;
        }

        public async Task<T> GetAsync<T>(string requestRelativeUrl, NameValueCollection httpParams)
        {
            var requestUrl = CreateRequestUri(requestRelativeUrl, httpParams);
            var response = await _httpClient.GetAsync(requestUrl, HttpCompletionOption.ResponseHeadersRead);
            response.EnsureSuccessStatusCode();
            var data = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(data);
        }

        private Uri CreateRequestUri(string relativePath, NameValueCollection httpParams)
        {
            Uri endpoint = new Uri(_baseUri, relativePath);
            endpoint = endpoint.SetQueryVal("apikey", _apiKey);
            foreach (var httpParam in httpParams.AllKeys)
            {
                string key = httpParam;
                string value = httpParams[key];
                endpoint = endpoint.SetQueryVal(key, value);
            }
            return endpoint;
        }

        private HttpContent CreateHttpContent<T>(T content)
        {
            var json = JsonConvert.SerializeObject(content, MicrosoftDateFormatSettings);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }
    }
}
