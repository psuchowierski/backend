﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using WarsawUM.Api.Common;
using WarsawUM.Api.Interfaces;
using WarsawUM.Api.Models;

namespace WarsawUM.Api.Services
{
    public class NotificationsRepository : INotificationsRepository
    {
        private readonly INotificationsApiClient _apiHttpClient;
        private const string threatsId = "28dc65ad-fff5-447b-99a3-95b71b4a7d1e";
        private const string getNotificationsUri = "/api/action/19115store_getNotificationsForDate";
        public NotificationsRepository(INotificationsApiClient apiHttpClient)
        {
            _apiHttpClient = apiHttpClient;
        }

        public async Task<IEnumerable<Notification>> GetNotifications(int days)
        {
            var dateFrom = DateTimeOffset.UtcNow.AddDays(days * -1).ToUnixTimeMilliseconds();
            var dateTo = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            var httpParams = new NameValueCollection();
            httpParams.Add("id", threatsId);
            httpParams.Add("dateFrom", dateFrom.ToString());
            httpParams.Add("dateTo", dateTo.ToString()); 

            var response = await _apiHttpClient.GetAsync<NotificationsRoot>(getNotificationsUri, httpParams);
            return response.Result.Result.Notifications;
        }
    }
}
